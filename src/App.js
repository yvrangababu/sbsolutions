import "./App.css";
import Header from "./components/Header/index";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import HomePage from "./components/HomePage/index";
import ServicesPage from "./components/Services/index";
import Payroll from "./components/Payroll/index";
import TaxConsultancy from "./components/TaxConsultancy/index";
import Contact from "./components/Contact/index";
import Footer from "./components/Footer";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />

        <Switch>
          <Route exact={true} path={["/", "/home"]} component={HomePage} />
          <Route path="/services" component={ServicesPage} />
          <Route path="/payroll" component={Payroll} />
          <Route path="/taxconsultancy" component={TaxConsultancy} />
          <Route path="/contact" component={Contact} />
        </Switch>

        <Footer />

      </BrowserRouter>
    </div>
  );
}

export default App;

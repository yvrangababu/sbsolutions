import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { Formik, Form, Field } from "formik";
import './Contact.scss';

function Contact(props) {
  return (
    <section className="mb-5">
      <Container fluid className="p-0">
        <Row className="justify-content-md-center px-4">
          <Col xs={12} md={8}>
            <div className="heading-section text-center mt-5">
              <h1>Let's Connect</h1>
              <h6 className="mb-5">
                Questions OR If You’d just want to say "HELLO"
              </h6>
            </div>
          </Col>
        </Row>

        <Row className="justify-content-md-center px-4">
        <Col xs={12} md={4} className="custom-form">
          <Formik
            initialValues={{
              name: "",
              email: "",
              comment: ""
            }}
            onSubmit={async values => {
              await new Promise(r => setTimeout(r, 500));
              alert(JSON.stringify(values, null, 2));
            }}
          >
            <Form>
              <div className="form-group mb-3">
                <Field
                  id="name"
                  name="name"
                  placeholder="Name"
                  className="form-control"
                />
              </div>

              <div className="form-group mb-3">
                <Field
                  id="email"
                  name="email"
                  placeholder="Email ID"
                  type="email"
                  className="form-control"
                />
              </div>
              <div className="form-group mb-3">
                <Field
                  component="textarea"
                  id="comment"
                  name="comment"
                  placeholder="Drop us a line..."
                  className="form-control"
                  rows="4" 
                />
              </div>

              <button type="submit" className="btn btn-primary custom-btn">Submit</button>
            </Form>
          </Formik>
          </Col>
        </Row>
      </Container>
    </section>
  );
}

export default Contact;

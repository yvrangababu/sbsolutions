import React from "react";
import { Container } from "react-bootstrap";
import "./Footer.scss";

function Footer(props) {
  return (
    <footer>
      <div className="copy-rights">
        <Container>
          Copyright &#169; 2013-20 Swift Business Solutions | One Point Solution
          for All Your Business Needs | All rights reserved
        </Container>
      </div>
    </footer>
  );
}

export default Footer;

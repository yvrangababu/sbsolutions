import React from "react";
import { Navbar, Nav } from "react-bootstrap";
import './Header.scss';
import {
  Link,
} from "react-router-dom";


function Header(props) {
  return (
    <header>
      <Navbar collapseOnSelect expand="lg" className="p-2">
        <Navbar.Brand to="/">SB Solutions</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-end">
          <Nav>
            <Link className="nav-link" to="/">Home</Link>
            <Link className="nav-link" to="/services">Services</Link>
            <Link className="nav-link" to="/payroll">Payroll</Link>
            <Link className="nav-link" to="/taxconsultancy">Tax Consultancy</Link>
            <Link className="nav-link" to="/contact">Contact</Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>


    </header>
  );
}

export default Header;

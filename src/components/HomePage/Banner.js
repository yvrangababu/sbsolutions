import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import BannerImage from "../../assets/banners/banner.jpeg";
import { HomePageContent } from "../../content/HomePage";

function Banner(props) {
  const desList = HomePageContent.BannerSection.descriptionPoints.map(item => (
    <p key={item.id}>{item.name}</p>
  ));

  return (
    <section className="p-5 banner-container">
      <Container fluid>
        <Row>
          <Col xs={12} md={8}>
            <div className="image-container">
              <img src={BannerImage} alt="Banner" className="img-fluid" />
            </div>
          </Col>
          <Col xs={12} md={4}>
            <div className="banner-section">
              <div className="heading-section mb-5">
                <h1>{HomePageContent.BannerSection.heading}</h1>
                <h6>{HomePageContent.BannerSection.subHeading}</h6>
              </div>
              <div className="des">{desList}</div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
}

export default Banner;

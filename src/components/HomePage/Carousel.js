import React from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { HomePageContent } from "../../content/HomePage";

function Carousel(props) {
  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  const visionList = HomePageContent.carouselSection.vison.map(item => (
    <h6 key={item.id}>{item.name}</h6>
  ));

  const missionList = HomePageContent.carouselSection.mission.map(item => (
    <h6 key={item.id}>{item.name}</h6>
  ));

  const qualityList = HomePageContent.carouselSection.quality.map(item => (
    <h6 key={item.id}>{item.name}</h6>
  ));

  return (
    <section className="carousel-section text-center">
      <Slider {...settings}>
        <div>
          <div className="heading-section text-center">
            <h1>Vision</h1>
            {visionList}
          </div>
        </div>
        <div>
        <div className="heading-section text-center">
            <h1>Our Mision</h1>
            {missionList}
          </div>
        </div>
        <div>
        <div className="heading-section text-center">
            <h1>Quality</h1>
            {qualityList}
          </div>
        </div>
      </Slider>
    </section>
  );
}

export default Carousel;

import React from "react";
import { Container, Row } from "react-bootstrap";
import { ServicesPageContent } from "../../content/Services";

function Services(props) {


  const servicesList = ServicesPageContent.Services.map(item => (
    <div key={item.id} className="service-item">{item.serviceName}</div>
  ));

  return (
    <section className="services px-5 mb-5">
      <Container fluid>
        <Row>
          <div className="heading-section text-center mb-5">
            <h1>{ServicesPageContent.heading}</h1>
            <h6>{ServicesPageContent.tagLine}</h6>
          </div>
          <div className="d-flex flex-wrap">
          {servicesList}
          </div>
        </Row>
      </Container>
    </section>
  );
}

export default Services;

import React from "react";
import "./Home.scss";
import Banner from "./Banner";
import Services from "./Services";
import Carousel from "./Carousel";

function HomePage(props) {
  return (
    <div>
      <Banner />
      <Carousel />
      <Services />
    </div>
  );
}

export default HomePage;

import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Payrollbanner from "../../assets/banners/payroll-banner.png";
import "./Payroll.scss";
import { PayrollContent } from "../../content/Payroll";

function Payroll(props) {
  const PTList = PayrollContent.ProfessionalTax.list.map(item => (
    <p key={item.id}>{item.name}</p>
  ));

  const ESI = PayrollContent.ESI.list.map(item => <p key={item.id}>{item.name}</p>);

  const EPF = PayrollContent.EPF.list.map(item => <p key={item.id}>{item.name}</p>);
  const ExtraNoteList = PayrollContent.ExtraNote.map(item => (
    <li key={item.id}>{item.name}</li>
  ));

  const payrollDes = PayrollContent.des.map(item => (
    <p key={item.id}>{item.name}</p>
  ));

  return (
    <section className="payroll mb-5">
      <Container fluid className="p-0">
        <Row className="justify-content-md-center px-4">
          <Col xs={12} md={8}>
            <div className="heading-section text-center mt-5">
              <h1>{PayrollContent.heading}</h1>
              <h6 className="mb-5">
                {PayrollContent.tagLine}
              </h6>

             {payrollDes}
            </div>
          </Col>
        </Row>

        <img src={Payrollbanner} alt="Payroll" className="img-fluid my-5" />

        <Row className="px-4">
          <Col xs={12} md={4}>
            <div className="payroll-item">
              <h3>{PayrollContent.EPF.name}</h3>
              {EPF}
            </div>
          </Col>

          <Col xs={12} md={4}>
            <div className="payroll-item">
              <h3>{PayrollContent.ESI.name}</h3>
              {ESI}
            </div>
          </Col>

          <Col xs={12} md={4}>
            <div className="payroll-item">
              <h3>{PayrollContent.ProfessionalTax.name}</h3>
              {PTList}
            </div>
          </Col>
        </Row>

        <Row className="note mt-5">
          <Col xs={12} md={7}>
            <div className="p-4">
              <ul>{ExtraNoteList}</ul>
            </div>
          </Col>

          <Col xs={12} md={4}>
            <div className="p-4">
              <h3>Talk to an Expert</h3>
              <ul className="connect">
                <li>
                  <a href="mailto:info@swiftbusinesssolutions.co.in">
                    info@swiftbusinesssolutions.co.in
                  </a>
                </li>
                <li>
                  <a href="tel:+91 9845-114-411">+91 9845-114-411</a>
                </li>
                <li>
                  <a href="tel:+91 9448-208-435">+91 9448-208-435</a>
                </li>
              </ul>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
}

export default Payroll;

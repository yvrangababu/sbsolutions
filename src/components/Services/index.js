import React from 'react'
import Services from "../HomePage/Services"
import ServicesBanner from "../../assets/banners/services-banner.png";


function ServicesPage(props) {
    return (
        <section>
        <img src={ServicesBanner} className="img-fluid mb-5" alt="ServiceBanner" />

        <Services />
        </section>
    )
}

export default ServicesPage;


import React from "react";
import { Container, Row, Col, Tab, Nav } from "react-bootstrap";
import "./TaxConsultancy.scss";
import { TaxConsultancyContent } from "../../content/TaxConsultancy";

function TaxConsultancy(props) {

  const gstList = TaxConsultancyContent.GST.map(item => (
    <li key={item.id}>{item.name}</li>
  ));

  const incomeTaxList = TaxConsultancyContent.IncomeTax.map(item => (
    <li key={item.id}>{item.name}</li>
  ));

  const TDSList = TaxConsultancyContent.TDS.map(item => (
    <li key={item.id}>{item.name}</li>
  ));

  const ProfessionalTaxList = TaxConsultancyContent.ProfessionalTax.map(item => (
    <li key={item.id}>{item.name}</li>
  ));


  return (
    <section className="mb-5">
      <Container fluid className="p-0">
        <Row className="justify-content-md-center px-4">
          <Col xs={12} md={8}>
            <div className="heading-section text-center mt-5">
              <h1>{TaxConsultancyContent.heading}</h1>
              <h6 className="mb-5">
               {TaxConsultancyContent.tagLine}
              </h6>
            </div>
          </Col>
        </Row>
      </Container>

      <Container className="p-0">
        <Row className="custom-tabs mt-5">
          <Tab.Container defaultActiveKey="gst">
            <Row>
              <Col sm={3} className="border-right">
                <Nav variant="pills" className="flex-column">
                  <Nav.Item>
                    <Nav.Link eventKey="gst">
                      Goods and Services Tax (GST)
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="it">Income Tax</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="tds">
                      Tax Deduction At Source (TDS)
                    </Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="pt">Professional Tax</Nav.Link>
                  </Nav.Item>
                </Nav>
              </Col>
              <Col sm={9}>
                <Tab.Content>
                  <Tab.Pane eventKey="gst">
                  <ul>{gstList}</ul>
                  </Tab.Pane>
                  <Tab.Pane eventKey="it">
                   <ul>{incomeTaxList}</ul>
                  </Tab.Pane>
                  <Tab.Pane eventKey="tds">
                  <ul>{TDSList}</ul>
                  </Tab.Pane>
                  <Tab.Pane eventKey="pt">
                  <ul>{ProfessionalTaxList}</ul>
                  </Tab.Pane>
                </Tab.Content>
              </Col>
            </Row>
          </Tab.Container>
        </Row>
      </Container>


    </section>
  );
}

export default TaxConsultancy;

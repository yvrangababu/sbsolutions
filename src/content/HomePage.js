const HomePageContent = {

// Banner Section Content 

  BannerSection: {
    heading: "Swift Business Solutions",
    subHeading: "One Point Solution for all your Business Needs",
    descriptionPoints: [
      {
        id: 1,
        name: "Comprehensive range of all Financial and compliance services."
      },
      {
        id: 2,
        name: "Experienced and enthusiastic team of professionals to ensure timely planning and execution."
      },
      {
        id: 3,
        name: "Planning, Positioning, and Organizing the Company to facilitate future growth and potential exit."
      },
      {
        id: 4,
        name: "Strict adherence to GAAP and Accounting standards thereby limiting any potential liability for Management and Board of Directors."
      },
      {
        id: 5,
        name: "A Higher level of service and competitive fees."
      }
    ]
  },


  //Carousel Content Vision, Mission, Quality

  carouselSection: {
    vison: [
      {
        id: 1,
        name: "To Provide one stop solution for all our Clients’ financial, taxation and compliance needs"
      }
    ],
    mission: [
      {
        id: 1,
        name: "To establish trust, Comfort and convenience as a one step business solutions provider"
      },
      {
        id: 2,
        name:"To provide quick, simple, effective and progressive solutions for business"
      }
    ],
    quality: [
      {
        id: 1,
        name: "We are committed to understand customer needs, expectations and continually providing simple, practical, user friendly and cost effective solutions."
      },
      {
        id: 2,
        name: "We accomplish this by providing products and services that meet or exceeds the expectations of our customers today and tomorrow by"
      }
    ]
  }
};

export { HomePageContent };

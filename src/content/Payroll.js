const PayrollContent = {
  heading: "Payroll",
  tagLine: "An integral part of each and every business organisations.",
  des: [
    {
      id: 1,
      name:
        "We at Swift Business Solutions, undertake payroll processing on behalf our clients which includes."
    },
    {
      id: 2,
      name:
        "New employee salary structure, issue of offer letter, appointment letter, consolidation of joining reports Updation of salary structure based on Transfer, promotion, increment etc Tax calculation for each employee and TDS deduction, TDS return preparation, issue of Form 16. Preparation of monthly salary payable working, statutory deductions etc"
    }
  ],


  //Professional Tax Content


  ProfessionalTax: {
    name: "Professional Tax",
    list: [
      {
        id: 1,
        name: "Obtaining registration under PT Act as and when required"
      },
      {
        id: 2,
        name: "Monthly professional tax deduction, payment and return filing"
      },
      { id: 3, name: "Annual return preparation and filing" },
      { id: 4, name: "Assisting client in PT assessment/audit proces" }
    ]
  },

  //Enrolment and compliances Content

  ESI: {
    name: "ESI -Enrolment and compliances",
    list: [
      { id: 1, name: "Employer registration and amendment" },
      { id: 2, name: "Employee IP Registration / previous IP linking" },
      { id: 3, name: "Addition / deletion of insured persons family members" },
      { id: 4, name: "Issue of Membership Cards" },
      { id: 5, name: "Monthly ECR uploading, challan generation and payment" },
      { id: 6, name: "Employee exit formalities" },
      { id: 7, name: "Assisting client in ESI inspection proces" }
    ]
  },


//Professional Tax Content

  EPF: {
    name: "EPF -Enrolment and compliances",
    list: [
      { id: 1, name: "Employer registration and amendment" },
      { id: 2, name: "New employee registration, linking previous member id" },
      { id: 3, name: "Nomination registration / modification" },
      { id: 4, name: "UAN generation / linking previous UAN" },
      {
        id: 5,
        name: "KYC report preparation/uploading KYC documents and compliance"
      },
      { id: 6, name: "Employee’s EPF account transfer and claim process" },
      { id: 7, name: "Monthly ECR uploading, challan generation and payment" },
      {
        id: 8,
        name:
          "Employee exit formalities and assisting employees in their claims"
      },
      { id: 9, name: "Assisting client in EPF inspection process" }
    ]
  },

  //Extra Note

  ExtraNote: [
    {
      id: 1,
      name:
        "Employee exit formalities -Final settlement calculation and other compliances"
    },
    { id: 2, name: "Employee’s ITR preparation and filing (optional)" },
    {
      id: 3,
      name:
        "Many more support services as and when required by the employer/client"
    }
  ]
};

export { PayrollContent };

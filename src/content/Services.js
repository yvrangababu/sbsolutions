const ServicesPageContent = {
  heading: "Services",
  tagLine: "We are here, If you are looking for your",
  Services: [
    {
      id: 1,
      serviceName: "Payroll and/or Accounts outsourcing"
    },
    {
      id: 2,
      serviceName: "GST registration, monthly or annual return filing & other compliances"
    },
    {
      id: 3,
      serviceName: "TDS deduction, payment, return filing"
    },
    {
      id: 4,
      serviceName: "Personal Income Tax"
    },
    {
      id: 5,
      serviceName: "Business Income Tax"
    },
    {
      id: 6,
      serviceName: "Book Keeping and Accounts Outsourcing"
    },
    {
      id: 7,
      serviceName: "ESI and EPF Registration and Compliances"
    },
    {
      id: 8,
      serviceName: "Professional Tax Registration and Compliances"
    },
    {
      id: 9,
      serviceName: "Registration under Shops & Establishments Act"
    },
    {
      id: 10,
      serviceName: "Many more business support services"
    }
  ]
};

export { ServicesPageContent };

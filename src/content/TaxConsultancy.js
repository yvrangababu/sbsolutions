const TaxConsultancyContent = {
    heading: "Tax Consultancy",
    tagLine: " Lorem Ipsum is simply dummy text of the printing and typesetting industry",
    GST: [
        { id: 1, name: "Registration -Preparation of application, consolidation of required documents, assisting departmental site inspection and obtaining TIN" },
        { id: 2, name: "Monthly Return : Scrutinising monthly input and output credit and monthly return preparation and uploading"},
        { id: 3, name: "Annual Returns : Preparation of annual returns, obtaining CA certificate and compliance under VAT Act" },
        { id: 4, name: "Statutory Forms : Issue and collection of statutory forms under VAT Act, maintenance of records" },
        { id: 5, name: "Audit : Attending VAT Audit / inspection, Preparation of documents/reports for VAT audit and responding departmental queries" },
        { id: 6, name: "Many more additional services as and when required by client" },
    ],
    IncomeTax: [
        { id: 1, name: "New PAN application / Reprint/amendment application" },
        { id: 2, name: "Personal and business income tax computation"},
        { id: 3, name: "Computation of advance tax liability" },
        { id: 4, name: "Tracking Tax deduction at source by clients and collection for TDS certificates" },
        { id: 5, name: "ITR preparation" },
        { id: 6, name: "Representing clients for their income tax assessment / scrutiny" },
        { id: 7, name: "Employees Tax calculation / return filing" },
    ],
    TDS: [
        { id: 1, name: "New TAN Application/Ammendment" },
        { id: 2, name: "Scrutinising Tax Deduction at source and payments"},
        { id: 3, name: "Quarterly return preparation / revised returns as and when required" },
        { id: 4, name: "Preparation and issue of Tax Deduction Certificates" },
    ],
    ProfessionalTax: [
        { id: 1, name: "Registration -Preparation of application, consolidation of required documents for Registation under PT Act" },
        { id: 2, name: "SRenewal/Ammendment/Surrender compliance"},
        { id: 3, name: "Annual payments" },
    ],
}


export { TaxConsultancyContent };